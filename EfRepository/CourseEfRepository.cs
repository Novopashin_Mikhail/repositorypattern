using System.Collections.Generic;
using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Core.OtusContext;

namespace EfRepository
{
    public class CourseEfRepository : OtusEfRepository<Course>, ICourseRepository
    {
        public CourseEfRepository(OtusDbContext db) : base(db)
        {
        }
        
        public override void Delete(int id)
        {
            var entity = _db.Course.Find(id);
            if (entity != null)
                _db.Course.Remove(entity);
        }

        public Course Get(int id)
        {
            return _db.Set<Course>().FirstOrDefault(x => x.Id == id);
        }

        public ICollection<Course> GetAllCourses()
        {
            return _db.Set<Course>().ToList();
        }
    }
}