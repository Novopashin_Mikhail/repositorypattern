using System.Collections.Generic;
using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Core.OtusContext;

namespace EfRepository
{
    public class UserEfRepository : OtusEfRepository<User>, IUserRepository
    {
        public UserEfRepository(OtusDbContext db) : base(db)
        {
        }
        
        public User Get(int id)
        {
            return _db.User.FirstOrDefault(x => x.Id == id);
        }

        public ICollection<User> GetAllStudents()
        {
            return _db.Set<User>().Where(x => x.IsTeacher == false).ToList();
        }

        public ICollection<User> GetAllTeachers()
        {
            return _db.Set<User>().Where(x => x.IsTeacher == true).ToList();
        }
        
        public override void Delete(int id)
        {
            var entity = _db.User.Find(id);
            if (entity != null)
                _db.User.Remove(entity);
        }
    }
}