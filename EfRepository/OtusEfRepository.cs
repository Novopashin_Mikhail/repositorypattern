﻿using System;
using Core;
using Core.OtusContext;
using Microsoft.EntityFrameworkCore;

namespace EfRepository
{
    public class OtusEfRepository<T> : IRepository<OtusDbContext, T>
    where T: class
    {
        public OtusEfRepository(OtusDbContext db)
        {
            _db = db;
        }
        
        private bool disposed = false;
        protected OtusDbContext _db;
        public virtual void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    _db.Dispose();
                }
            }
            this.disposed = true;
        }
 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Create(T item)
        {
            _db.Set<T>().Add(item);
        }

        public void Update(T item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public virtual void Delete(int id)
        {
        }
    }
}