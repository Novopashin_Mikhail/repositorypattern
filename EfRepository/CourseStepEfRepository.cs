using System.Collections.Generic;
using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Core.OtusContext;

namespace EfRepository
{
    public class CourseStepEfRepository : OtusEfRepository<CourseStep>, ICourseStepRepository
    {
        public CourseStepEfRepository(OtusDbContext db) : base(db)
        {
        }
        
        public override void Delete(int id)
        {
            var entity = _db.CourseStep.Find(id);
            if (entity != null)
                _db.CourseStep.Remove(entity);
        }

        public CourseStep Get(int id)
        {
            return _db.Set<CourseStep>().FirstOrDefault(x => x.Id == id);
        }

        public ICollection<CourseStep> GetAllStepByCourse(int courseId)
        {
            return _db.Set<CourseStep>().Where(x => x.CourseId == courseId).ToList();
        }
    }
}