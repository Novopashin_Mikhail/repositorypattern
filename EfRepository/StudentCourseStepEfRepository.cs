using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Core.OtusContext;

namespace EfRepository
{
    public class StudentCourseStepEfRepository : OtusEfRepository<StudentCourseStep>, IStudentCourseStepRepository
    {
        public StudentCourseStepEfRepository(OtusDbContext db) : base(db)
        {
        }
        
        public override void Delete(int id)
        {
            var entity = _db.StudentCourseStep.Find(id);
            if (entity != null)
                _db.StudentCourseStep.Remove(entity);
        }

        public StudentCourseStep Get(int id)
        {
            return _db.Set<StudentCourseStep>().FirstOrDefault(x => x.Id == id);
        }
    }
}