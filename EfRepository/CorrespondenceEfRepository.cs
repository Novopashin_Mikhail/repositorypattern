using System.Collections.Generic;
using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Core.OtusContext;

namespace EfRepository
{
    public class CorrespondenceEfRepository : OtusEfRepository<Correspondence>, ICorrespondenceRepository
    {
        public CorrespondenceEfRepository(OtusDbContext db) : base(db)
        {
        }

        public Correspondence Get(int id)
        {
            return _db.Set<Correspondence>().FirstOrDefault(x => x.Id == id);
        }

        public ICollection<Correspondence> GetCorrespondenceByStudentCourseStep(int studentCourseStepId)
        {
            return _db.Set<Correspondence>().Where(x => x.StudentCourseStepId == studentCourseStepId).ToList();
        }
        
        public override void Delete(int id)
        {
            var entity = _db.Correspondence.Find(id);
            if (entity != null)
                _db.Correspondence.Remove(entity);
        }
    }
}