using Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace EfRepository.Configuration
{
    public class ConfigureEfRepository
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, EfUnitOfWork>();
            services.AddScoped(typeof(OtusEfRepository<>), typeof(OtusEfRepository<>));
            services.AddScoped<IUserRepository, UserEfRepository>();
            services.AddScoped<ICourseRepository, CourseEfRepository>();
            services.AddScoped<ICourseStepRepository, CourseStepEfRepository>();
            services.AddScoped<IStudentCourseStepRepository, StudentCourseStepEfRepository>();
            services.AddScoped<ICorrespondenceRepository, CorrespondenceEfRepository>();
        }
    }
}