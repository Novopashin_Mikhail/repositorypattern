using System;
using Core.Interfaces;
using Core.OtusContext;

namespace EfRepository
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private static OtusDbContext _db;
        
        public EfUnitOfWork(OtusDbContext db)
        {
            _db = db;
        }
        
        readonly Lazy<UserEfRepository> _lazyUserEfRepository = new Lazy<UserEfRepository>(() => new UserEfRepository(_db));
        readonly Lazy<CourseEfRepository> _lazyCourseEfRepository = new Lazy<CourseEfRepository>(() => new CourseEfRepository(_db));
        readonly Lazy<StudentCourseStepEfRepository> _lazyStudentCourseStepEfRepository = new Lazy<StudentCourseStepEfRepository>(() => new StudentCourseStepEfRepository(_db));
        readonly Lazy<CourseStepEfRepository> _lazyCourseStepEfRepository = new Lazy<CourseStepEfRepository>(() => new CourseStepEfRepository(_db));
        readonly Lazy<CorrespondenceEfRepository> _lazyCorrespondenceEfRepository = new Lazy<CorrespondenceEfRepository>(() => new CorrespondenceEfRepository(_db));
        
        public IUserRepository GetUserRepository => _lazyUserEfRepository.Value;
        public ICourseRepository GetCourseRepository => _lazyCourseEfRepository.Value;
        public IStudentCourseStepRepository GetStudentCourseStepRepository => _lazyStudentCourseStepEfRepository.Value;
        public ICourseStepRepository GetCourseStepRepository => _lazyCourseStepEfRepository.Value;
        public ICorrespondenceRepository GetCorrespondenceRepository => _lazyCorrespondenceEfRepository.Value;
    }
}