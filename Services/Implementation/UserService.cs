using Core.Dto;
using Core.Interfaces;
using Services.Interfaces;

namespace Services.Implementation
{
    public class UserService : IUserService
    {
        private IUnitOfWork _unitOfWork;
        
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        public UserDto Get(int id)
        {
            var userRepository = _unitOfWork.GetUserRepository;
            var resultDb = userRepository.Get(id);
            var dto = new UserDto
            {
                Name = resultDb.Fio
            };
            return dto;
        }
    }
}