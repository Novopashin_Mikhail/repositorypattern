using Core.Dto;

namespace Services.Interfaces
{
    public interface IUserService
    {
        UserDto Get(int id);
    }
}