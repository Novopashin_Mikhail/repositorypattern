using Core.Dto;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;

namespace RepositoryPattern.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        
        [HttpGet("GetById")]
        public IActionResult GetById([FromQuery] int userId)
        {
            return Ok(_userService.Get(userId));
        }
    }
}