using Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace DapperRepository.Configuration
{
    public class ConfigureDapperRepository
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, DapperUnitOfWork>();
            services.AddScoped(typeof(OtusDapperRepository<>), typeof(OtusDapperRepository<>));
            services.AddScoped<IUserRepository, UserDapperRepository>();
            services.AddScoped<ICourseRepository, CourseDapperRepository>();
            services.AddScoped<ICourseStepRepository, CourseStepDapperRepository>();
            services.AddScoped<IStudentCourseStepRepository, StudentCourseStepDapperRepository>();
            services.AddScoped<ICorrespondenceRepository, CorrespondenceDapperRepository>();
        }
    }
}