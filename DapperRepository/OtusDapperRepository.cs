﻿using System;
using System.Data;
using System.Linq;
using Core;
using Core.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace DapperRepository
{
    public class OtusDapperRepository<T> : IOtusDapperRepository<T>
        where T: BaseEntity
    {
        protected string ConnectionString;
        
        public OtusDapperRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("OtusDb");
        }
        
        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(ConnectionString);
            }
        }
        
        public T Get(int id, string entityName)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<T>($"SELECT * FROM \"{entityName}\" WHERE \"Id\" = @ID", new {Id = id})
                    .FirstOrDefault();
            }
        }

        public void Upsert(T item, string query)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query(query, item);
            }
        }

        public void Delete(int id, string entityName)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute($"DELETE FROM \"{entityName}\" WHERE \"Id\"=@Id", new { Id = id });
            }
        }
    }
}