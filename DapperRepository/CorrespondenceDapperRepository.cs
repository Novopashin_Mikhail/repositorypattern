using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace DapperRepository
{
    public class CorrespondenceDapperRepository : OtusDapperRepository<Correspondence>, ICorrespondenceRepository
    {
        private const string EntityName = "Correspondence";
        private const string InsertQuery = @"INSERT INTO Correspondence (Id, Message, UserId, StudentCourseStepId, CreatedAt) VALUES (@Id, @Message, @UserId, @StudentCourseStepId, @CreatedAt)";
        private const string UpdateQuery = @"Update Correspondence SET Message=@Message, UserId=@UserId, StudentCourseStepId=@StudentCourseStepId, CreatedAt=@CreatedAt, Name=@Name WHERE Id=@Id";
        
        public CorrespondenceDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }
        
        public void Create(Correspondence item)
        {
            base.Upsert(item, InsertQuery);
        }

        public void Update(Correspondence item)
        {
            base.Upsert(item, UpdateQuery);
        }
        
        public void Delete(int id)
        {
            base.Delete(id, EntityName);
        }

        public Correspondence Get(int id)
        {
            return base.Get(id, EntityName);
        }

        public ICollection<Correspondence> GetCorrespondenceByStudentCourseStep(int studentCourseStepId)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<Correspondence>($"SELECT * FROM \"Correspondence\" WHERE \"StudentCourseStepId\" = @StudentCourseStepId", new {StudentCourseStepId = studentCourseStepId})
                    .ToList();
            }
        }
    }
}