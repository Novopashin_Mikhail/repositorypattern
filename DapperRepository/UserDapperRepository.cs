using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace DapperRepository
{
    public class UserDapperRepository : OtusDapperRepository<User>, IUserRepository
    {
        private const string UserEntityName = "User";
        private const string InsertQuery = @"INSERT INTO User (Id, Email, Fio, IsTeacher) VALUES (@Id, @Email, @Fio, @IsTeacher)";
        private const string UpdateQuery = @"Update User SET Email=@Email, Fio=@Fio, IsTeacher=@IsTeacher WHERE Id=@Id";
        
        public UserDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public void Create(User item)
        {
            base.Upsert(item, InsertQuery);
        }

        public void Update(User item)
        {
            base.Upsert(item, UpdateQuery);
        }

        public void Delete(int id)
        {
            base.Delete(id, UserEntityName);
        }

        public User Get(int id)
        {
           return base.Get(id, UserEntityName);
        }

        public ICollection<User> GetAllStudents()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<User>($"SELECT * FROM \"User\" WHERE \"IsTeacher\"=false")
                    .ToList();
            }
        }

        public ICollection<User> GetAllTeachers()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<User>($"SELECT * FROM \"Course\" WHERE \"IsTeacher\"=true")
                    .ToList();
            }
        }
    }
}