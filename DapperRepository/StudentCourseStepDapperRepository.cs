using Core.Entity;
using Core.Interfaces;
using Microsoft.Extensions.Configuration;

namespace DapperRepository
{
    public class StudentCourseStepDapperRepository : OtusDapperRepository<StudentCourseStep>, IStudentCourseStepRepository
    {
        private const string EntityName = "StudentCourseStep";
        private const string InsertQuery = @"INSERT INTO StudentCourseStep (Id, UserId, CourseStepId, Rate, StartedAt, EndedAt) VALUES (@Id, @UserId, @CourseStepId, @Rate, @StartedAt, @EndedAt)";
        private const string UpdateQuery = @"Update StudentCourseStep SET UserId=@UserId, CourseStepId=@CourseStepId, Rate=@Rate, StartedAt=@StartedAt, EndedAt=@EndedAt WHERE Id=@Id";
        
        public StudentCourseStepDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }
        
        public void Create(StudentCourseStep item)
        {
            base.Upsert(item, InsertQuery);
        }

        public void Update(StudentCourseStep item)
        {
            base.Upsert(item, UpdateQuery);
        }
        
        public void Delete(int id)
        {
            base.Delete(id, EntityName);
        }

        public StudentCourseStep Get(int id)
        {
            return base.Get(id, EntityName);
        }
    }
}