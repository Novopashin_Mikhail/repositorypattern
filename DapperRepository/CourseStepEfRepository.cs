using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace DapperRepository
{
    public class CourseStepDapperRepository : OtusDapperRepository<CourseStep>, ICourseStepRepository
    {
        private const string EntityName = "CourseStep";
        private const string InsertQuery = @"INSERT INTO CourseStep (Id, CourseId, Name) VALUES (@Id, @CourseId, @Name)";
        private const string UpdateQuery = @"Update CourseStep SET CourseId=@CourseId, Name=@Name WHERE Id=@Id";
        
        public CourseStepDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }
        
        public void Create(CourseStep item)
        {
            base.Upsert(item, InsertQuery);
        }

        public void Update(CourseStep item)
        {
            base.Upsert(item, UpdateQuery);
        }
        
        public void Delete(int id)
        {
            base.Delete(id, EntityName);
        }

        public CourseStep Get(int id)
        {
            return base.Get(id, EntityName);
        }

        public ICollection<CourseStep> GetAllStepByCourse(int courseId)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<CourseStep>($"SELECT * FROM \"CourseStep\" WHERE \"CourseId\"=@CourseId", new {CourseId = courseId})
                    .ToList();
            }
        }
    }
}