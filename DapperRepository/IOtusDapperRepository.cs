﻿using System;
using Core;
using Core.Entity;
using Microsoft.Extensions.Configuration;

namespace DapperRepository
{
    public interface IOtusDapperRepository<T>
    where T: BaseEntity
    {
        T Get(int id, string entityName);
        void Upsert(T item, string query);
        void Delete(int id, string entityName);
    }
}