using System;
using Core.Interfaces;
using Microsoft.Extensions.Configuration;

namespace DapperRepository
{
    public class DapperUnitOfWork : IUnitOfWork
    {
        private static IConfiguration _configuration;
        
        public DapperUnitOfWork(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        readonly Lazy<UserDapperRepository> _lazyUserDapperRepository = new Lazy<UserDapperRepository>(() => new UserDapperRepository(_configuration));
        readonly Lazy<CourseDapperRepository> _lazyCourseDapperRepository = new Lazy<CourseDapperRepository>(() => new CourseDapperRepository(_configuration));
        readonly Lazy<StudentCourseStepDapperRepository> _lazyStudentCourseStepDapperRepository = new Lazy<StudentCourseStepDapperRepository>(() => new StudentCourseStepDapperRepository(_configuration));
        readonly Lazy<CourseStepDapperRepository> _lazyCourseStepDapperRepository = new Lazy<CourseStepDapperRepository>(() => new CourseStepDapperRepository(_configuration));
        readonly Lazy<CorrespondenceDapperRepository> _lazyCorrespondenceDapperRepository = new Lazy<CorrespondenceDapperRepository>(() => new CorrespondenceDapperRepository(_configuration));
        
        public IUserRepository GetUserRepository => _lazyUserDapperRepository.Value;
        public ICourseRepository GetCourseRepository => _lazyCourseDapperRepository.Value;
        public IStudentCourseStepRepository GetStudentCourseStepRepository => _lazyStudentCourseStepDapperRepository.Value;
        public ICourseStepRepository GetCourseStepRepository => _lazyCourseStepDapperRepository.Value;
        public ICorrespondenceRepository GetCorrespondenceRepository => _lazyCorrespondenceDapperRepository.Value;
    }
}