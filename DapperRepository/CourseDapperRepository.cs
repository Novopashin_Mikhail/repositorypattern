using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Entity;
using Core.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace DapperRepository
{
    public class CourseDapperRepository : OtusDapperRepository<Course>, ICourseRepository
    {
        private const string EntityName = "Course";
        private const string InsertQuery = @"INSERT INTO Course (Id, Name, Description) VALUES (@Id, @Name, @Description)";
        private const string UpdateQuery = @"Update Course SET Name=@Name, Description=@Description WHERE Id=@Id";
        
        public CourseDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }
        
        public void Create(Course item)
        {
            base.Upsert(item, InsertQuery);
        }

        public void Update(Course item)
        {
            base.Upsert(item, UpdateQuery);
        }
        
        public void Delete(int id)
        {
            base.Delete(id, EntityName);
        }

        public Course Get(int id)
        {
            return base.Get(id, EntityName);
        }

        public ICollection<Course> GetAllCourses()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<Course>($"SELECT * FROM \"Course\" ")
                    .ToList();
            }
        }
    }
}