namespace Core.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository GetUserRepository { get; }
        IStudentCourseStepRepository GetStudentCourseStepRepository { get; }
        ICourseRepository GetCourseRepository { get; }
        ICourseStepRepository GetCourseStepRepository { get; }
        ICorrespondenceRepository GetCorrespondenceRepository { get; }
    }
}