using System.Collections;
using System.Collections.Generic;
using Core.Entity;

namespace Core.Interfaces
{
    public interface IUserRepository
    {
        void Create(User item); 
        void Update(User item); 
        void Delete(int id);
        User Get(int id);

        ICollection<User> GetAllStudents();
        ICollection<User> GetAllTeachers();
    }
}