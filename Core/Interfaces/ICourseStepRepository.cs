using System.Collections;
using System.Collections.Generic;
using Core.Entity;

namespace Core.Interfaces
{
    public interface ICourseStepRepository
    {
        void Create(CourseStep item); 
        void Update(CourseStep item); 
        void Delete(int id);
        CourseStep Get(int id);

        ICollection<CourseStep> GetAllStepByCourse(int courseId);
    }
}