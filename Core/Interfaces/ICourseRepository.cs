using System.Collections;
using System.Collections.Generic;
using Core.Entity;

namespace Core.Interfaces
{
    public interface ICourseRepository
    {
        void Create(Course item); 
        void Update(Course item); 
        void Delete(int id);
        Course Get(int id);
        ICollection<Course> GetAllCourses();
    }
}