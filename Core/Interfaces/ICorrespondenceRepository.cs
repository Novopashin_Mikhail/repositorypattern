using System.Collections.Generic;
using Core.Entity;

namespace Core.Interfaces
{
    public interface ICorrespondenceRepository
    {
        void Create(Correspondence item); 
        void Update(Correspondence item); 
        void Delete(int id);
        Correspondence Get(int id);
        ICollection<Correspondence> GetCorrespondenceByStudentCourseStep(int studentCourseStepId);
    }
}