using Core.Entity;

namespace Core.Interfaces
{
    public interface IStudentCourseStepRepository
    {
        void Create(StudentCourseStep item); 
        void Update(StudentCourseStep item); 
        void Delete(int id);
        StudentCourseStep Get(int id);
    }
}