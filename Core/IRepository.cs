using System;

namespace Core
{
    public interface IRepository<TContext, TEntity> : IDisposable
        where TContext: class
        where TEntity: class
    {
        void Create(TEntity item); 
        void Update(TEntity item); 
        void Delete(int id); 
        void Save();
    }
}