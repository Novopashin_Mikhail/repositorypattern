using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Entity
{
    public class CourseStep : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string Name { get; set; }
        
        public Course Course { get; set; }
        public ICollection<StudentCourseStep> StudentCourseSteps { get; set; }
    }
}