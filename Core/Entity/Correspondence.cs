using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Entity
{
    public class Correspondence : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Message { get; set; }
        public int UserId { get; set; }
        public int StudentCourseStepId { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public StudentCourseStep StudentCourseStep { get; set; }
    }
}