using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Entity
{
    public class StudentCourseStep : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CourseStepId { get; set; }
        public int? Rate { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime? EndedAt { get; set; }

        public User User { get; set; }
        public CourseStep CourseStep { get; set; }
        public ICollection<Correspondence> Correspondences { get; set; }
    }
}