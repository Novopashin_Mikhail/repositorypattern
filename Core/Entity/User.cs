using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Entity
{
    public class User : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Fio { get; set; }
        public bool IsTeacher { get; set; }
        
        public ICollection<StudentCourseStep> StudentCourseSteps { get; set; }
    }
}