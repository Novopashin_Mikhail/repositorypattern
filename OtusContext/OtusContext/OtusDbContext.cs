using Core.Entity;
using EfRepository.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Core.OtusContext
{
    public class OtusDbContext : DbContext
    {
        public OtusDbContext(DbContextOptions<OtusDbContext> options) : base(options)
        {
        }
        
        public DbSet<Correspondence> Correspondence { get; set; }
        public DbSet<Course> Course { get; set; }
        public DbSet<CourseStep> CourseStep { get; set; }
        public DbSet<StudentCourseStep> StudentCourseStep { get; set; }
        public DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new CourseConfiguration());
            modelBuilder.ApplyConfiguration(new CourseStepConfiguration());
            modelBuilder.ApplyConfiguration(new StudentCourseStepConfiguration());
            modelBuilder.ApplyConfiguration(new CorrespondenceConfiguration());
        }
    }
}