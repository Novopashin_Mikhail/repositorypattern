﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EfRepository.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Course",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Course", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Email = table.Column<string>(nullable: true),
                    Fio = table.Column<string>(nullable: true),
                    IsTeacher = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CourseStep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CourseId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseStep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CourseStep_Course_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Course",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentCourseStep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(nullable: false),
                    CourseStepId = table.Column<int>(nullable: false),
                    Rate = table.Column<int>(nullable: true),
                    StartedAt = table.Column<DateTime>(nullable: false),
                    EndedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCourseStep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentCourseStep_CourseStep_CourseStepId",
                        column: x => x.CourseStepId,
                        principalTable: "CourseStep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourseStep_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Correspondence",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Message = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    StudentCourseStepId = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Correspondence", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Correspondence_StudentCourseStep_StudentCourseStepId",
                        column: x => x.StudentCourseStepId,
                        principalTable: "StudentCourseStep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Course",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, null, "Разработчик C#" },
                    { 2, null, "Архитектор высоких нагрузок" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Email", "Fio", "IsTeacher" },
                values: new object[,]
                {
                    { 1, "test1@email.ru", "Петров Петр Петрович", false },
                    { 2, "test2@email.ru", "Васильев Василий Васильевич", true }
                });

            migrationBuilder.InsertData(
                table: "CourseStep",
                columns: new[] { "Id", "CourseId", "Name" },
                values: new object[,]
                {
                    { 1, 1, "Знакомство" },
                    { 2, 1, "Операторы" },
                    { 3, 1, "Рефлексия" },
                    { 4, 2, "Начало" },
                    { 5, 2, "Середина" },
                    { 6, 2, "Конец" }
                });

            migrationBuilder.InsertData(
                table: "StudentCourseStep",
                columns: new[] { "Id", "CourseStepId", "EndedAt", "Rate", "StartedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2019, 10, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, new DateTime(2019, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 2, 2, null, null, new DateTime(2019, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 }
                });

            migrationBuilder.InsertData(
                table: "Correspondence",
                columns: new[] { "Id", "CreatedAt", "Message", "StudentCourseStepId", "UserId" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 10, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Задание отправил", 1, 1 },
                    { 2, new DateTime(2019, 10, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Отлично, задание проверено, все хорошо", 1, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Correspondence_StudentCourseStepId",
                table: "Correspondence",
                column: "StudentCourseStepId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseStep_CourseId",
                table: "CourseStep",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseStep_CourseStepId",
                table: "StudentCourseStep",
                column: "CourseStepId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseStep_UserId",
                table: "StudentCourseStep",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Correspondence");

            migrationBuilder.DropTable(
                name: "StudentCourseStep");

            migrationBuilder.DropTable(
                name: "CourseStep");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Course");
        }
    }
}
