using Core.OtusContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace EfRepository
{
    public class ContextFactory : IDesignTimeDbContextFactory<OtusDbContext>
    {
        public OtusDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<OtusDbContext>();
            optionsBuilder.UseNpgsql(
                "Server=localhost;Port=5432;Database=repositorypattern;User Id=postgres;Password=321678;Integrated Security=true;Pooling=true;");
            return new OtusDbContext(optionsBuilder.Options);
        }
    }
}