using System;
using Core.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfRepository.Configuration
{
    public class CorrespondenceConfiguration : IEntityTypeConfiguration<Correspondence>
    {
        public void Configure(EntityTypeBuilder<Correspondence> builder)
        {
            builder
                .HasOne(c => c.StudentCourseStep)
                .WithMany(scs => scs.Correspondences);

            builder.HasData(
                new Correspondence
                {
                    Id = 1,
                    Message = "Задание отправил",
                    UserId = 1,
                    StudentCourseStepId = 1,
                    CreatedAt = new DateTime(2019, 10, 3),
                },
                new Correspondence
                {
                    Id = 2,
                    Message = "Отлично, задание проверено, все хорошо",
                    UserId = 2,
                    StudentCourseStepId = 1,
                    CreatedAt = new DateTime(2019, 10, 6)
                }
            );
        }
    }
}