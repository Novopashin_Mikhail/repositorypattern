using Core.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfRepository.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasData(
                new User
                {
                    Id = 1,
                    Email = "test1@email.ru",
                    Fio = "Петров Петр Петрович",
                    IsTeacher = false
                },
                new User
                {
                    Id = 2,
                    Email = "test2@email.ru",
                    Fio = "Васильев Василий Васильевич",
                    IsTeacher = true
                }
            );
        }
    }
}