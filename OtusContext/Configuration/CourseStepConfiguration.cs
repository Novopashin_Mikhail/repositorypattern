using Core.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfRepository.Configuration
{
    public class CourseStepConfiguration : IEntityTypeConfiguration<CourseStep>
    {
        public void Configure(EntityTypeBuilder<CourseStep> builder)
        {
            builder
                .HasOne(cs => cs.Course)
                .WithMany(c => c.CourseSteps)
                .HasForeignKey(c => c.CourseId);
            
            builder.HasData(
                new CourseStep
                {
                    Id = 1,
                    CourseId = 1,
                    Name = "Знакомство"
                },
                new CourseStep
                {
                    Id = 2,
                    CourseId = 1,
                    Name = "Операторы"
                },
                new CourseStep
                {
                    Id = 3,
                    CourseId = 1,
                    Name = "Рефлексия"
                },
                new CourseStep
                {
                    Id = 4,
                    CourseId = 2,
                    Name = "Начало"
                },
                new CourseStep
                {
                    Id = 5,
                    CourseId = 2,
                    Name = "Середина"
                },
                new CourseStep
                {
                    Id = 6,
                    CourseId = 2,
                    Name = "Конец"
                }
            );
        }
    }
}