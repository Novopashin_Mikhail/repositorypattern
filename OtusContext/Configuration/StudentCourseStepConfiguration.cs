using System;
using Core.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfRepository.Configuration
{
    public class StudentCourseStepConfiguration : IEntityTypeConfiguration<StudentCourseStep>
    {
        public void Configure(EntityTypeBuilder<StudentCourseStep> builder)
        {
            builder
                .HasOne(scs => scs.User)
                .WithMany(u => u.StudentCourseSteps)
                .HasForeignKey(u => u.UserId);

            builder
                .HasOne(scs => scs.CourseStep)
                .WithMany(cs => cs.StudentCourseSteps)
                .HasForeignKey(cs => cs.CourseStepId);
            
            builder.HasData(
                new StudentCourseStep
                {
                    Id = 1,
                    UserId = 1,
                    CourseStepId = 1,
                    Rate = 5,
                    StartedAt = new DateTime(2019, 10, 1),
                    EndedAt = new DateTime(2019, 10, 6)
                },
                new StudentCourseStep
                {
                    Id = 2,
                    UserId = 1,
                    CourseStepId = 2,
                    StartedAt = new DateTime(2019, 10, 1)
                }
            );
        }
    }
}